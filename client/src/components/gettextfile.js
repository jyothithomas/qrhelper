import React, { useEffect, useState } from 'react';
import { withRouter, Link, useHistory } from 'react-router-dom'
import '../styles/main-styles.css';
import Axios from 'axios';
import AuthApi from "./AuthApi";
import Cookies from 'js-cookie';
import { NavDropdown } from 'react-bootstrap';

function GetTextFile() {
    const BASE_URL = "http://reprolog.net:5001/api/"
    const [lot, setlotNumber] = useState('');
    const [fullName, setFullName] = useState('');
    const [UniqueCodelist, setuniqueCodeList] = useState([]);
    const [downloadLink, setDownloadLink] = useState('')
    const [status, setStatus] = useState('');
    


    useEffect(() => {
        Axios.get(BASE_URL +"login").then((response) => {
            if (response.data.loggedIn === true) {
                setFullName(response.data.user[0].fullName);
            }
        })
    }, []);

    const Auth = React.useContext(AuthApi)
    Axios.defaults.withCredentials = true;
   
    const handleOnClick = () => {
        Auth.updateStatus(false);
        Cookies.remove("user");
        window.localStorage.removeItem('LOGGEDIN');
    }
    const readCookie = () => {
        const user = Cookies.get("user");
        if (user) {
            //   setAuth(true);
        }
    }
    useEffect(() => {
        readCookie();
    }, [])

    let history = useHistory();

    const submitLot = event => {
        console.log(lot);
        Axios.get(BASE_URL + 'get', {
            params:
              { lot: lot }
          }).then((res) => {
            setuniqueCodeList(res.data);
            console.log(res.data)
            var data= ''
            var items = [];
            res.data.map((val) =>{
                items.push (val.unique_code);
            })
            console.log("Items---"+items);
            if( items.length > 0 ) {
            data = new Blob([items.join('\n')], { type: 'text/plain' })
            if (downloadLink !== '') window.URL.revokeObjectURL(downloadLink)
            setDownloadLink(window.URL.createObjectURL(data))
            //console.log("data",data,UniqueCodelist)
            let a = document.getElementById("download")
          //  a.download = 'codes.txt'
           // a.href = downloadLink
           
            a.click();
            }else {
                setStatus("please enter a valid lotNumber");
            }
          }).catch(error => {
            console.log(" not able to get data" + error)
          })
    }
    const refreshStop = document.getElementById('getfile');
    if (refreshStop) {
        refreshStop.addEventListener('click', function (e) {
            e.preventDefault();
        });
    }
    
    const handleQrHelper = () => {
        Auth.updateStatus(true);
        history.push('/qr-helper')
    }

    const handleAddDrug = () => {
        Auth.updateStatus(true);
        history.push('/adddrug')
    }
      const handleAddClient = () => {
        Auth.updateStatus(true);
        history.push('/addclient');
    }
    return (
        <div>
            <nav className="navbar navbar-expand navbar-light fixed-top">
                <div className="container">

                    <Link className="navbar-brand" to={'/'}>Home</Link>
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">

                                <NavDropdown title={fullName} className="navDropDown" style={{ color: "black" }}>
                                    <NavDropdown.Item>
                                        <Link onClick={handleOnClick} to={'/'} className="nav-link">Logout</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item>
                                        <Link onClick={handleAddClient} className="nav-link">Add New Client</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item>
                                        <Link onClick={handleAddDrug} className="nav-link">Add New Chemical Indicator</Link>
                                    </NavDropdown.Item> 
                                    <NavDropdown.Item>
                                        <Link onClick={handleQrHelper} className="nav-link">Reprolog</Link>
                                    </NavDropdown.Item>

                                </NavDropdown>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <form>
            <div className="auth-inner-inner">
                <h3>Search with a Lot Number</h3>
                <div className="form-group">
                    <label>Lot Number</label>
                    <input id="lotnumber" type="text" className="form-control" placeholder="Search here"
                        value={lot} onChange={(e) => {
                            setlotNumber(e.target.value);
                        }} />
                </div>
        
                <button id="getfile" onClick={() => { submitLot(); validate(); }} className="btn btn-primary btn-block">Download unique codes </button>
               <br/>
                <h5>{status}</h5>
                </div></form>
                
                <a id="download" download='codes.txt' href={downloadLink}> </a>
        </div>
        
    )
    function validate() {
        let lotnum = document.getElementById("lotnumber");
        if (lotnum.value === "") {
            setStatus("Please enter a lot number");
            return false;
        }
        else {
            setStatus("File downloaded successfully");
            return true;
        }
    }
                    }
export default withRouter(GetTextFile);