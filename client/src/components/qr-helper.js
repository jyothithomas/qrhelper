import React, { useEffect, useState, useContext } from 'react'
import '../styles/main-styles.css';
import Axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { withRouter, Link, useHistory } from 'react-router-dom'
import AuthApi from "./AuthApi";
import { NavDropdown } from 'react-bootstrap';
import Cookies from 'js-cookie';

//this
function QrHelper() {
  const MAX_NUM_CODE = 1000;
  const [fullName, setFullName] = useState('');

  const BASE_URL = "http://reprolog.net:5001/api/"
  const [drug_code, setName] = useState('');
  const [client_code, setAddress] = useState('');
  const [counter, setCount] = useState('');
  //const [id,setId] = useState('');
  const [UniqueCodeliststart, setuniqueCodeListstart] = useState([]);
  const [UniqueCodelistend, setuniqueCodeListend] = useState([]);
  const [UniqueCodelist, setuniqueCodeList] = useState([]);
  const [UniqueCodelistDrug, setuniqueCodeListDrug] = useState([]);
  const [UniqueCodelistClient, setuniqueCodeListClient] = useState([]);
  const [newLoad, setNewLoad] = useState(false);
  let history = useHistory();

  useEffect(() => {
    fetchListData();
  }, []);
  Axios.defaults.withCredentials = true;

  useEffect(() => {
    Axios.get(BASE_URL +"login").then((response) => {
      console.log(response.data.data + "res data")
      if (response.data.loggedIn === true) {
        setFullName(response.data.user[0].fullName);
      }
    })
  }, []);

  useEffect(() => {
    const data = window.localStorage.getItem('LOGGEDIN');
    Auth.updateStatus(JSON.parse(Auth.isAuthenticated));
}, [])

useEffect(() => {
    window.localStorage.setItem('LOGGEDIN',JSON.stringify(Auth.isAuthenticated))
}, []);


  const Auth = React.useContext(AuthApi)

  const handleLogOut = () => {
    Auth.updateStatus(false);
    window.localStorage.removeItem('LOGGEDIN');
  }

  const handleOnClick = () => {
    Auth.updateStatus(false);
    Cookies.remove("user");
  }
  const readCookie = () => {
    const user = Cookies.get("user");
    if (user) {
      // setAuth(true);
    }
  }
  useEffect(() => {
    readCookie();
  }, [])

  const reprolog_btn = () => {
    history.push("/qr-helper");

  }

  const filter_btn = () => {
    history.push("/filter-qrhelper");

  }

  const handleAddClient = () => {
    Auth.updateStatus(true);
    history.push('/addclient');
}

const handleAddDrug = () => {
  Auth.updateStatus(true);
history.push('/adddrug');
}
const handleFileWrite = () => {
  Auth.updateStatus(true);
  history.push('/gettextfile')
}
  const fetchListData = () => {
    Axios.get(BASE_URL + 'getdrug').then((response) => {
      setuniqueCodeListDrug(response.data)
    });

    Axios.get(BASE_URL + 'getclient').then((response) => {
      setuniqueCodeListClient(response.data)
    });
  }

  const fetchData = () => {
    console.log("I am getting called")
    Axios.get(BASE_URL + 'get')
      .then((response) => {
        setuniqueCodeList(response.data)
        console.log("data on hand")
        setNewLoad(false)
      }).catch(err => {
        console.log(err + " Not able to fetch data")
        setNewLoad(false)
      })

    Axios.get(BASE_URL + 'getend')
      .then((response) => {
        setuniqueCodeListend(response.data)
        console.log("data on hand")
        setNewLoad(false)
      }).catch(err => {
        console.log(err + " Not able to fetch data")
        setNewLoad(false)
      })

    Axios.get(BASE_URL + 'getstart')
      .then((response) => {
        setuniqueCodeListstart(response.data)
        console.log("data on hand")
        setNewLoad(false)
      }).catch(err => {
        console.log(err + " Not able to fetch data")
        setNewLoad(false)
      })
    // return(
    //   UniqueCodelist.map((val)=>{
    //     return <h4> {val.unique_code} </h4>
    //    }
    // ))

  }
  const submitDetails = event => {
    event.preventDefault();
    setNewLoad(true);
    Axios.post(BASE_URL + 'insert', {
      drug: drug_code,
      client: client_code,
      count: counter,
      //id: id
      //unique: UniqueCodelist.drug_code
    }).then((res) => {
      console.log("got the data")
      // setNewLoad(false)
    }).catch(error => {
      console.log(" not able to get data" + error)
    })
  }



  return (

    <div>
      <nav className="navbar navbar-expand navbar-light fixed-top">
        <div className="container">

          <Link className="navbar-brand" to={'/'}>Home</Link>
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">

                <NavDropdown title={fullName} className="navDropDown" style={{ color: "black" }}>
                  <NavDropdown.Item>
                    <Link onClick={handleLogOut} to={'/'} className="nav-link">Logout</Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link onClick={handleAddClient} className="nav-link">Add New Client</Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                  <Link onClick={handleAddDrug} className="nav-link">Add New Chemical Indicator</Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                  <Link onClick={handleFileWrite} className="nav-link">Download Text File</Link>
                  </NavDropdown.Item>
                </NavDropdown>
              </li>
            </ul>
          </div>
        </div>

      </nav>


      <div class="card text-center">
        <div class="card-header">
          <ul class="nav nav-pills card-header-pills">
            <li class="nav-item">
              <a class="nav-link active" onClick={reprolog_btn}>Reprolog Main</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onClick={filter_btn}> &nbsp; &nbsp; &nbsp;Filter and Search</a>
            </li>
          </ul>
        </div>
      </div>
      <br />
      <div className="auth-inner-inner">
      <h3 style={{ textAlign: "center" }}> Unique Code Generator For ReproLog</h3>
      <div className="form-group col-md-10">
        <label htmlFor="Drug Code"> <b>Chemical Indicator</b></label>
        <select className="form-control" value={drug_code}
          onChange={(e) => setName(e.target.value)}>
          <option value="⬇️ Select a Drug ⬇️"> -- Select -- </option>
          {UniqueCodelistDrug.map((val) => <option value={val.drug_code}>{val.drug_namel}</option>)}
        </select>
      </div>
      <div className="form-group col-md-10">
        <label htmlFor="Client Code"><b>Client</b><span></span></label>
        <select className="form-control" value={client_code}
          onChange={(e) => setAddress(e.target.value)}>
          <option value="⬇️ Select a Client ⬇️"> -- Select a client -- </option>
          {UniqueCodelistClient.map((val) => <option value={val.client_code}>{val.client_name}</option>)}
        </select>
      </div>
      <div className="form-group col-md-10">
        <label htmlFor="No:of Codes" ><b>No. of codes</b></label>
        <select className="form-control" value={counter}
          onChange={(e) => setCount(e.target.value)}>
          <option value="Select the Count"> -- Select a count --</option>
          {Array.from(new Array(MAX_NUM_CODE), (counter, index) => index + 1).map(value => <option key={value} value={value}>{value}</option>)}
        </select>
      </div>
      <br />
      <div class="d-flex justify-content-center">
      <button className="btn btn-primary btn-block" onClick={(e) => submitDetails(e)}> Submit </button></div>
      <div>
        <br />
        {newLoad === true ? fetchData() : (
          UniqueCodeliststart.map((val) => {
            return <h3 style={{ textAlign: "center" }}> <h6 style={{ color: 'black' }}><u>Unique code start:</u></h6>{val.unique_code}</h3>
          }
          ))
        }
        {newLoad === true ? fetchData() : (
          UniqueCodelistend.map((val) => {
            return <h3 style={{ textAlign: "center" }}> <h6 style={{ color: 'black' }}><u>Unique code end:</u></h6>{val.unique_code}</h3>
          }
          ))
        }
        {newLoad === true ? fetchData() : (
          UniqueCodelistend.map((val) => {
            return <h3 style={{ textAlign: "center" }}> <h6 style={{ color: 'black' }}><u>Lot number:</u></h6><mark>{val.lot_number}</mark>  </h3>
          }
          ))
        }
      </div>
    </div>
    </div>
  );
}

export default withRouter(QrHelper);