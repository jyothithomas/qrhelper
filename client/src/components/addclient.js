import React, { useEffect, useState } from 'react';
import { withRouter, Link, useHistory } from 'react-router-dom'
import '../styles/main-styles.css';
import Axios from 'axios';
import AuthApi from "./AuthApi";
import Cookies from 'js-cookie';
import { NavDropdown } from 'react-bootstrap';

function AddClient() {
    const BASE_URL = "http://reprolog.net:5001/api/"
    const [client_name_add, setclientName] = useState('');
    const [client_code_add, setclientCode] = useState('');
    const [fullName, setFullName] = useState('');
    const [status, setStatus] = useState('');
    useEffect(() => {
        Axios.get(BASE_URL+"login").then((response) => {
            if (response.data.loggedIn === true) {
                setFullName(response.data.user[0].fullName);
            }
        })
    }, []);

    const Auth = React.useContext(AuthApi)
    Axios.defaults.withCredentials = true;
   
    const handleOnClick = () => {
        Auth.updateStatus(false);
        Cookies.remove("user");
        window.localStorage.removeItem('LOGGEDIN');
    }
    const readCookie = () => {
        const user = Cookies.get("user");
        if (user) {
            //   setAuth(true);
        }
    }
    useEffect(() => {
        readCookie();
    }, [])

    let history = useHistory();

    const submitClient = (e) => {
        //e.preventDefault();
        Axios.post(BASE_URL+'insertclient', {
            client_name: client_name_add,
            client_code: client_code_add,
        }).then((response) => {
            console.log(response);
        });
    };
    const refreshStop = document.getElementById('addclient');
    if (refreshStop) {
        refreshStop.addEventListener('click', function (e) {
            e.preventDefault();
        });
    }

    // const handleAddClient = () => {
    //     Auth.updateStatus(true);
    //     history.push('/addclient');
    // }
    
    const handleQrHelper = () => {
        Auth.updateStatus(true);
        history.push('/qr-helper')
    }

    const handleAddDrug = () => {
        Auth.updateStatus(true);
        history.push('/adddrug')
    }
    const handleFileWrite = () => {
        Auth.updateStatus(true);
        history.push('/gettextfile')
    }
    return (
        <div>
            <nav className="navbar navbar-expand navbar-light fixed-top">
                <div className="container">

                    <Link className="navbar-brand" to={'/'}>Home</Link>
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">

                                <NavDropdown title={fullName} className="navDropDown" style={{ color: "black" }}>
                                    <NavDropdown.Item>
                                        <Link onClick={handleOnClick} to={'/'} className="nav-link">Logout</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item>
                                        <Link onClick={handleAddDrug} className="nav-link">Add New Chemical Idicator</Link>
                                    </NavDropdown.Item> 
                                    <NavDropdown.Item>
                                        <Link onClick={handleFileWrite} className="nav-link">Download Text File</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item>
                                        <Link onClick={handleQrHelper} className="nav-link">Reprolog</Link>
                                    </NavDropdown.Item>

                                </NavDropdown>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <form>
            <div className="auth-inner-inner">
                <h3>Add a new Client</h3>
                <div className="form-group">
                    <label>Client Name</label>
                    <input id="clientname" type="text" className="form-control" placeholder="Client Name"
                        onChange={(e) => {
                            setclientName(e.target.value);
                        }} />
                </div>
                <div className="form-group">
                    <label>Client Code</label>
                    <input id="clientcode" type="text" className="form-control" placeholder="Client Code"
                        onChange={(e) => {
                            setclientCode(e.target.value);
                        }} />
                </div>
                <button id="addclient" onClick={() => { submitClient(); validate(); }} className="btn btn-primary btn-block">Add Client </button>
                <h5>{status}</h5>
                </div>
                </form>
        </div>
    )
    function validate() {
        let client = document.getElementById("clientname");
        if (client.value === "") {
            setStatus("Please enter client name");
            return false;
        }
        let code = document.getElementById("clientcode");
        if (code.value === "") {
            setStatus("Please enter a valid code");
            return false;
        }
        else {
            setStatus("New Client Added")
            return true;
        }
    }

}
export default withRouter(AddClient);