import React, {useEffect,useState} from 'react';
import {withRouter , Link, useHistory} from 'react-router-dom'
import '../styles/main-styles.css';
import Axios from 'axios';
import AuthApi from "./AuthApi";
import Cookies from 'js-cookie';
import { NavDropdown } from 'react-bootstrap';



function AddDrug()
{
    const BASE_URL = "http://reprolog.net:5001/api/"
    const [auth,setAuth] = useState(false);
    const [drug_name_add,setdrugName] = useState('');
    const [drug_code_add,setdrugCode] = useState('');
    const [fullName,setFullName] = useState('');
    const [status,setStatus] = useState('');
    useEffect(()=> {
        Axios.get(BASE_URL+"login").then((response)=> {
            if (response.data.loggedIn === true){
                setFullName(response.data.user[0].fullName);
            }
        })
      },[]);
const Auth = React.useContext(AuthApi)
Axios.defaults.withCredentials = true;
const handleOnClick = () => {
    Auth.updateStatus(false);
    window.localStorage.removeItem('LOGGEDIN');
    Cookies.remove("user");
}
const readCookie = () => {
    const user = Cookies.get("user");
    if (user) {
        //   setAuth(true);
    }
}
useEffect(() => {
    readCookie();
}, [])
let history = useHistory();
     const submitDrug = (e) => {
     Axios.post(BASE_URL+'insertdrug', {
         drug_name: drug_name_add,
         drug_code: drug_code_add,
     }).then((response)=> {
         console.log(response);
     });
 };
 const refreshStop = document.getElementById('adddrug');
 if(refreshStop){
     refreshStop.addEventListener('click', function(e){
         e.preventDefault();
     });
 }
 const handleQrHelper = () => {
    Auth.updateStatus(true);
    history.push('/qr-helper')
}
const handleaddClient = () => {
    Auth.updateStatus(true);
    history.push('/qr-helper')
}
const handleFileWrite = () => {
    Auth.updateStatus(true);
    history.push('/gettextfile')
}
    return(
        <div>
        <nav className="navbar navbar-expand navbar-light fixed-top">
                 <div className="container">
                   
                   <Link  className="navbar-brand" to={'/'}>Home</Link>
                   <div className="collapse navbar-collapse">
                     <ul className="navbar-nav ml-auto">
                     <li className="nav-item">
                       
                  <NavDropdown title= {fullName}  className="navDropDown"  style={{color: "black"}}>
                   <NavDropdown.Item>
                   <Link onClick={handleOnClick} to={'/'} className="nav-link">Logout</Link>
                   </NavDropdown.Item>
                   <NavDropdown.Item>
                   <Link onClick={handleaddClient} to={'/addclient'} className="nav-link">Add New Client</Link>
                   </NavDropdown.Item>
                   <NavDropdown.Item>
                        <Link onClick={handleFileWrite} className="nav-link">Download Text File</Link>
                    </NavDropdown.Item>
                   <NavDropdown.Item>
                    <Link onClick={handleQrHelper} to={'/qr-helper'} className="nav-link">Reprolog</Link>
                    </NavDropdown.Item>
                   
                  </NavDropdown>
                     </li>
                     </ul>    
               </div>
               </div>
               </nav>
<form>
<div className="auth-inner-inner">
<h3>Add a new Chemical Indicator</h3>
<div className="form-group">
    <label>Chemical Indicator Name</label>
    <input id = "drugname" type="text" className="form-control" placeholder="Chemical Indicator Name"
    onChange={(e) => {setdrugName(e.target.value);
    }}/>
</div>
<div className="form-group">
    <label>Chemical Indicator Code</label>
    <input id="drugcode" type="text" className="form-control" placeholder="Chemical Indicator Code"
     onChange={(e) => {setdrugCode(e.target.value);
     }}/>
</div>
<button id = "adddrug" onClick={() => { submitDrug(); validate(); }} className="btn btn-primary btn-block">Add Chemical Indicator </button>
<h5>{status}</h5>
</div>
</form>
</div>
)
function validate()
{
 let drug = document.getElementById("drugname");
 console.log(drug.value)
 if( drug.value==="") 
 {
  setStatus("Please enter a name");
  return false;
 }
 let code = document.getElementById( "drugcode");
 if( code.value==="")
 {
  setStatus("Please enter a valid code");
  return false;
 }
 else
 {
    setStatus("New Drug Added")
  return true;
 }
}

}
export default withRouter(AddDrug);